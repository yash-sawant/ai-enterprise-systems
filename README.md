# AI Enterprise Systems



## Description

This repository consists of various lab assignments for the course AI in Enterprise Systems offered in the Artificial Intelligence program at Durham College, Canada.

## Getting Started

Each branch in this repository contains a certain assignment or part of a larger assignment.